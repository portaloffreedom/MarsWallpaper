uniform mat4 u_MVMatrix;
uniform mat4 u_MVPMatrix;
uniform mat4 u_NormalMatrix;
attribute vec4 v_Position;
attribute vec2 v_TextureCoordinate;

varying vec3 v_pos;
varying vec3 v_normal;
varying vec3 v_lightDirection;
varying vec2 v_out_textureCoordinate;

// radius of mars is 1.0 in this program
// distance(mars,sun) / radius(mars) = 73757.1913261543
// 73757.1913261543 = sqrt(2) * 104308.4202959946
#define DISTANCE_TIMES_SQRT_2 104308.4202959946

const vec3 lightPosition = vec3(-DISTANCE_TIMES_SQRT_2, 0.0, DISTANCE_TIMES_SQRT_2);

void main() {
    vec4 v_position4 = u_MVMatrix * v_Position;
    vec3 v_position3 = v_position4.xyz / v_position4.w;
    vec3 normal = v_Position.xyz;

    v_pos = v_Position.xyz / v_Position.w;
    v_normal = (u_NormalMatrix * vec4(normal, 1.0)).xyz;
    v_lightDirection = normalize(lightPosition - v_position3);

    v_out_textureCoordinate = v_TextureCoordinate;
    gl_Position = u_MVPMatrix * v_Position;
}