precision mediump float;
uniform sampler2D u_ColorTexture;
uniform sampler2D u_NormalTexture;

varying vec3 v_pos;
varying vec3 v_normal;
varying vec3 v_lightDirection;
varying vec2 v_out_textureCoordinate;

#define M_PI 3.1415926535897932384626433832795
void main() {

    // Texture coordinate
    vec3 v_color = texture2D(u_ColorTexture, v_out_textureCoordinate).rgb;
    vec2 sampled_normal = texture2D( u_NormalTexture, v_out_textureCoordinate ).rg*2.0 - 1.0;
    sampled_normal *= vec2(-1.0, 1.0);

    // light
    float orizon_closeness = 0.04/tan(v_normal.z+0.1 *M_PI/2.0);
    vec3 v_normal = normalize(
         vec3(sampled_normal, 0.0) + v_normal
    );
    float diff = max(0.00, dot(v_normal, v_lightDirection));
    vec3 color = diff * v_color;// + vec3(0.1, 0.1, 0.1);
    color.r = max(0.0, color.r - orizon_closeness);
    color.g = max(0.0, color.g - orizon_closeness);
    color.b = max(0.0, color.b - orizon_closeness);

    gl_FragColor = vec4(color, 1.0);
}