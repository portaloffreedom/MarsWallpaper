precision mediump float;
uniform sampler2D u_ColorTexture;

varying vec3 v_pos;
varying vec2 v_out_textureCoordinate;

void main() {
    vec3 v_color = texture2D(u_ColorTexture, v_out_textureCoordinate).rgb/1.5;
    gl_FragColor = vec4(v_color, 1.0);
}