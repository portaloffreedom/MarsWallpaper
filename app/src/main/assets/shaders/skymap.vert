uniform mat4 u_MVPMatrix;
attribute vec4 v_Position;
attribute vec2 v_TextureCoordinate;

varying vec3 v_pos;
varying vec2 v_out_textureCoordinate;

void main() {
    v_out_textureCoordinate = v_TextureCoordinate;
    gl_Position = u_MVPMatrix * v_Position;
}