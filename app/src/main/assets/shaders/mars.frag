precision mediump float;
uniform sampler2D u_ColorTexture;
uniform sampler2D u_NormalTexture;

varying vec3 v_pos;
varying vec3 v_normal;
varying vec3 v_lightDirection;
varying vec2 v_out_textureCoordinate;

#define M_PI 3.1415926535897932384626433832795
void main() {

    // Texture coordinate
    vec3 v_color = texture2D(u_ColorTexture, v_out_textureCoordinate).rgb;
    vec2 sampled_normal = texture2D( u_NormalTexture, v_out_textureCoordinate ).rg*2.0 - 1.0;
    sampled_normal *= vec2(-1.0, 1.0);

    // light
    float orizon_closeness = 0.04/tan(v_normal.z+0.1 *M_PI/2.0);
    vec3 v_normal = normalize(
         vec3(sampled_normal, 0.0) + v_normal
    );
    float diff = max(0.00, dot(v_normal, v_lightDirection));
    vec3 color = diff * v_color;// + vec3(0.1, 0.1, 0.1);
    color.r = max(0.0, color.r - orizon_closeness);

    vec3 reflection = normalize(
        reflect(-v_lightDirection, v_normal)
    );

    float spec = max(0.0, dot(v_normal, reflection));

    // if diffuse light is zero, don't even bother with the pow function
    if (diff != 0.0) {
        float reflectiveness = min(0.99, 0.6 + 0.5*(1.0 - (color.r)/(color.r + color.g + color.b)));
        float fSpec = pow(spec * reflectiveness, 64.0);
//        float fSpec = pow(spec, 128.0);
        color += vec3(fSpec, fSpec, fSpec);
    }

    gl_FragColor = vec4(color, 1.0);
}