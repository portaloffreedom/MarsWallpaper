uniform mat4 u_MVPMatrix;
attribute vec4 v_Position;
void main() {
    gl_Position = u_MVPMatrix * v_Position;
}