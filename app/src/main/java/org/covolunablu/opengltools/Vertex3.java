package org.covolunablu.opengltools;

/**
 * Created by matteo on 10/9/17.
 */

public class Vertex3 {
    public static final short SIZE = 3;
    public float x;
    public float y;
    public float z;

    public Vertex3() {
        this(0,0,0);
    }

    public Vertex3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public Vertex3 clone() throws CloneNotSupportedException {
        return new Vertex3(x, y, z);
    }

    public Vertex3 add(Vertex3 other) {
        return new Vertex3(
                this.x + other.x,
                this.y + other.y,
                this.z + other.z
        );
    }

    public Vertex3 sub(Vertex3 other) {
        return new Vertex3(
                this.x - other.x,
                this.y - other.y,
                this.z - other.z
        );
    }

    public Vertex3 mul(Vertex3 other) {
        return new Vertex3(
                this.x * other.x,
                this.y * other.y,
                this.z * other.z
        );
    }

    public Vertex3 div(Vertex3 other) {
        return new Vertex3(
                this.x / other.x,
                this.y / other.y,
                this.z / other.z
        );
    }

    public Vertex3 add(float v) {
        return new Vertex3(
                this.x + v,
                this.y + v,
                this.z + v
        );
    }

    public Vertex3 sub(float v) {
        return new Vertex3(
                this.x - v,
                this.y - v,
                this.z - v
        );
    }

    public Vertex3 mul(float v) {
        return new Vertex3(
                this.x * v,
                this.y * v,
                this.z * v
        );
    }

    public Vertex3 div(float v) {
        return new Vertex3(
                this.x / v,
                this.y / v,
                this.z / v
        );
    }

    public float dot(final Vertex3 other) {
        return (this.x * other.x)
                + (this.y * other.y)
                + (this.z * other.z);
    }
}