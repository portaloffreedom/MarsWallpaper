package org.covolunablu.marswallpaper;

import android.opengl.GLSurfaceView;

public class MarsWallpaperService extends OpenGLES2WallpaperService {
    @Override
    GLSurfaceView.Renderer getNewRenderer() {
        return new MyGLRenderer(this);
    }
}
