package org.covolunablu.marswallpaper;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

/**
 * Created by matteo on 10/8/17.
 */

public class MyGLSurfaceView extends GLSurfaceView {

    private MyGLRenderer mRenderer;

    public MyGLSurfaceView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public MyGLSurfaceView(Context context) {
        super(context);
        init();
    }

    private void init()
    {

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);
        setEGLConfigChooser(8, 8, 8, 8, 16, 8);

        mRenderer = new MyGLRenderer(this.getContext());

        // Set renderer
        setRenderer(mRenderer);
        // Render the view only when there is a change in the drawing data
        //setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public void setOffset(float xOffset, float yOffset) {
        mRenderer.setOffset(xOffset, yOffset);
    }
}
