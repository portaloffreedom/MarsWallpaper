package org.covolunablu.marswallpaper.objects;

import android.app.ActivityManager;
import android.content.Context;
import android.opengl.GLES20;
import android.preference.PreferenceManager;
import android.util.Log;

import org.covolunablu.marswallpaper.MyGLRenderer;
import org.covolunablu.marswallpaper.R;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by matteo on 10/11/17.
 */

public class SkyMap extends RenderableObject
{
    private static final String TAG = "SkyMap";
    protected final FloatBuffer textCoordBuffer;
    private         int         starmap_res;

    private int mColorTextureUniformHandle;
    private int mColorTextureDataHandle;

    public static SkyMap GenerateSkyMap(final Context context)
    {
        String skyMapResString = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString("starmap_res", "2");
        return GenerateSkyMap(context, skyMapResString);
    }

    public static SkyMap GenerateSkyMap(final Context context, String skyMapResString)
    {
        float phi = (float) ((1.0d + Math.sqrt(5.0f)) / 2.0d);    //1.618
        float du  = (float) (1.0d / Math.sqrt(phi * phi + 1.0d)); //0.526
        float dv  = phi * du; // 0.85065

        float[] vertexArray = {
                0.0f, dv, du, // 0
                0.0f, dv, -du, // 1
                0.0f, -dv, du, // 2
                0.0f, -dv, -du, // 3
                du, 0.0f, dv, // 4
                -du, 0.0f, dv, // 5
                du, 0.0f, -dv, // 6
                -du, 0.0f, -dv, // 7
                dv, du, 0.0f, // 8
                dv, -du, 0.0f, // 9
                -dv, du, 0.0f, // 10
                -dv, -du, 0.0f, // 11
        };

        short[] indexArray = {
                0, 1, 8,
                0, 4, 5,
                0, 5, 10,
                0, 8, 4,
                0, 10, 1,
                1, 6, 8,
                1, 7, 6,
                1, 10, 7,
                2, 3, 11,
                2, 4, 9,
                2, 5, 4,
                2, 9, 3,
                2, 11, 5,
                3, 7, 11,
                3, 6, 7,
                3, 9, 6,
                4, 8, 9,
                5, 11, 10,
                6, 9, 8,
                7, 10, 11,
                };


        VertexList vertexList = new VertexList(vertexArray, indexArray);

        vertexList.subdivide(3);
        vertexList.fixTextureCoordinates();

        return new SkyMap(
                context,
                vertexList.generateVertexData(),
                vertexList.generateTextCoordData(),
                vertexList.generateIndexesData(),
                skyMapResString
        );
    }

    private SkyMap(Context context,
                   float[] vertexArray,
                   float[] textCoordArray,
                   short[] indexArray
    )
    {
        this(
                context,
                vertexArray,
                textCoordArray,
                indexArray,
                PreferenceManager.getDefaultSharedPreferences(context)
                                 .getString("starmap_res", "2")
        );
    }

    private SkyMap(Context context,
                   float[] vertexArray,
                   float[] textCoordArray,
                   short[] indexArray,
                   String skyMapResString
    )
    {
        super(vertexArray, indexArray);
        initShaderProgram(context);

        // initialize uv coordinates byte buffer
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinates values * 4 bytes per float)
                textCoordArray.length * 4
        );
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        textCoordBuffer = bb.asFloatBuffer();
        // ad the colors to the FloatBuffer
        textCoordBuffer.put(textCoordArray);
        // set the buffer to read the first coordinate
        textCoordBuffer.position(0);

        setSkymapRes(context, skyMapResString);
    }

    public void setSkymapRes(Context context, String newValue)
    {
        int starmap_res;
        try {
            starmap_res = Integer.parseInt(newValue);
        } catch (NumberFormatException ignored) {
            starmap_res = 2;
        }

        setSkymapRes(context, starmap_res);
    }

    public void setSkymapRes(Context context, int newValue)
    {
        if (newValue == starmap_res) {
            return;
        }
        starmap_res = newValue;

        int max_mimap_res = 4;

        ActivityManager.MemoryInfo memoryInfo      = new ActivityManager.MemoryInfo();
        ActivityManager            activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager != null) {
            activityManager.getMemoryInfo(memoryInfo);
            double availableMegs = memoryInfo.availMem / 0x100000L;

            Log.d(TAG, "Available Megabytes: " + availableMegs);
            if (availableMegs < 100) {
                max_mimap_res = 2;
            } else if (availableMegs < 200) {
                max_mimap_res = 3;
            } else {
                max_mimap_res = 4;
            }

        }

        int targetResource;
        starmap_res = Math.min(starmap_res, max_mimap_res);
        switch (starmap_res) {
            case 0:
                Log.e(TAG, "Should not create the star map in the first place");
                return;
            default:
                Log.e(TAG, "Not recognized starmap resolution value, defaulting to 1k");
            case 1:
                targetResource = R.mipmap.starmap_1k;
                break;
            case 2:
                targetResource = R.mipmap.starmap_2k;
                break;
            case 3:
                targetResource = R.mipmap.starmap_3k;
                break;
            case 4:
                targetResource = R.mipmap.starmap_4k;
                break;
        }

        mColorTextureDataHandle = MyGLRenderer.loadTexture(context, targetResource);

    }

    @Override
    String GetVertexShaderCode(final Context context)
    {
        return getShaderCodeFromAsset(context, "shaders/skymap.vert");
    }

    @Override
    String GetFragmentShaderCode(final Context context)
    {
        return getShaderCodeFromAsset(context, "shaders/skymap.frag");
    }

    @Override
    protected void populateHandles()
    {
        // attributes
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "v_Position");
        mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgram, "v_TextureCoordinate");
        // uniforms
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVPMatrix");
        mColorTextureUniformHandle = GLES20.glGetUniformLocation(mProgram, "u_ColorTexture");
    }

    static final    int COORDS_PER_TEXTURE_COORD = 2;
    protected final int textCoordStride          = COORDS_PER_TEXTURE_COORD * 4; // 4 bytes per float

    @Override
    public void draw(float[] MVMatrix, float[] MVPMatrix, float[] NormalMatrix)
    {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);
        // get handle to vertex shader's vPosition member

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(
                mPositionHandle,
                COORDS_PER_VERTEX,
                GLES20.GL_FLOAT,
                false,
                vertexStride,
                vertexBuffer
        );
        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);
        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(
                mTextureCoordinateHandle,
                COORDS_PER_TEXTURE_COORD,
                GLES20.GL_FLOAT,
                false,
                textCoordStride,
                textCoordBuffer
        );

        // Set texture
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0); // Set the active texture unit to texture unit 0
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mColorTextureDataHandle);
        GLES20.glUniform1i(mColorTextureUniformHandle, 0);

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, MVPMatrix, 0);

        // Draw the triangle
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, vertexCount, GLES20.GL_UNSIGNED_SHORT, drawListBuffer);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
        GLES20.glDisableVertexAttribArray(mTextureCoordinateHandle);
    }
}
