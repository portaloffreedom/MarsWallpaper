package org.covolunablu.marswallpaper.objects;

import org.covolunablu.androidtools.Tuple;
import org.covolunablu.marswallpaper.BuildConfig;
import org.covolunablu.opengltools.Vertex3;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by matteo on 10/9/17.
 */

public class VertexList {
    private static final String TAG = "VertexList";
    private Vector<Vertex3> vertices;
    private Vector<Vertex3> textCoord;
    private Vector<Short>   indexes;

    private Map<Tuple<Short>, Short> slerpCache;

    public VertexList(float[] vertices, short[] indexes) {
        this.vertices = new Vector<>(vertices.length / 3);
        this.textCoord = new Vector<>(vertices.length / 3);
        this.indexes = new Vector<>(indexes.length, 3);

        slerpCache = new HashMap<>();

        if (BuildConfig.DEBUG) {
            if (vertices.length % Vertex3.SIZE != 0) throw new AssertionError();
            if (indexes.length % 3 != 0) throw new AssertionError();
        }

        for (int i = 0; i < vertices.length; i += 3) {
            Vertex3 pos = new Vertex3(vertices[i], vertices[i + 1], vertices[i + 2]);
            this.vertices.add(pos);
            this.textCoord.add(TextureCoordinateFromPos(pos));
        }

        for (short index : indexes) {
            this.indexes.add(index);
        }
    }

    static private float[] GenerateData(final Vector<Vertex3> source, boolean use_z) {
        short vectorSize = Vertex3.SIZE;
        if (!use_z)
            vectorSize--;

        float[] data = new float[source.size() * vectorSize];
        int     i    = 0;
        for (Vertex3 vertex : source) {
            data[i++] = vertex.x;
            data[i++] = vertex.y;
            if (use_z)
                data[i++] = vertex.z;
        }

        if (BuildConfig.DEBUG) {
            if (i != data.length) throw new AssertionError();
        }
        return data;
    }

    public float[] generateVertexData() {
        return GenerateData(vertices, true);
    }

    public float[] generateTextCoordData() {
        return GenerateData(textCoord, false);
    }

    public short[] generateIndexesData() {
        short[] data = new short[indexes.size()];
        int     i    = 0;
        for (Short index : indexes) {
            data[i++] = index;
        }
        if (BuildConfig.DEBUG) {
            if (i != data.length) throw new AssertionError();
        }
        return data;
    }

    static private Vertex3 TextureCoordinateFromPos(Vertex3 pos) {
        Vertex3 coordinates = new Vertex3();
        coordinates.y = -(pos.y + 1.0f) / 2.0f;

        float radius = (float) Math.sqrt(pos.x*pos.x + pos.z*pos.z);
        if (radius == 0) {
            coordinates.x = 0.0f;
            return coordinates;
        }
        float cos_coord = pos.x / radius;

        final float LIMIT = 1;
        if (cos_coord > LIMIT)
            cos_coord = LIMIT;
        else if (cos_coord < -LIMIT)
            cos_coord = -LIMIT;

            coordinates.x = (float) (Math.acos(cos_coord)/(2.0*Math.PI));
        if (pos.z < 0) {
            coordinates.x = 1.0f - coordinates.x;
        }

        return coordinates;
    }

    static private Vertex3 Slerp(Vertex3 p0, Vertex3 p1, float t) {
        float omega = (float) Math.acos(p0.dot(p1));

        Vertex3 ret = p0.mul((float) Math.sin((1.0f - t) * omega));
        ret = ret.add(p1.mul((float) Math.sin(t * omega)));
        ret = ret.mul((float) (1 / Math.sin(omega)));

        return ret;
    }

    private short cachedMidSlerp(short p0, short p1) {
        // need to reorder, otherwise it will be always a cache miss
        if (p0 < p1) {
            short tmp = p0;
            p0 = p1;
            p1 = tmp;
        }

        Tuple<Short> key = new Tuple<>(new Short[]{p0, p1});
        Short cachedElement = slerpCache.get(key);
        if (cachedElement != null) {
            //Log.v(TAG, "("+p0+","+p1+") slerp Cache hit!");
            return cachedElement;
        }
        //Log.v(TAG, "("+p0+","+p1+") slerp Cache miss!");

        Vertex3 newVertex = Slerp(vertices.elementAt(p0), vertices.elementAt(p1), 0.5f);
        short index = (short) vertices.size();
        vertices.add(newVertex);
        textCoord.add(TextureCoordinateFromPos(newVertex));

        slerpCache.put(key, index);
        return index;
    }

    private static short[] ConcatTriangleLists(short[][] listOfLists){
        int size = 0;
        for (short[] list: listOfLists) {
            size += list.length;
        }
        short[] triangleList = new short[size];

        int pos = 0;
        for (short[] list: listOfLists) {
            System.arraycopy(list, 0, triangleList, pos, list.length);
            pos += list.length;
        }

        if (BuildConfig.DEBUG) {
            if (pos != size) throw new AssertionError("pos != size");
        }

        return triangleList;
    }

    private short[] subdivideTriangle(short A, short B, short C, int depth) {
        if (depth == 0) {
            return new short[]{A, B, C};
        }
        depth--;

        short ab_mid = cachedMidSlerp(A, B);
        short bc_mid = cachedMidSlerp(B, C);
        short ca_mid = cachedMidSlerp(C, A);

        short[] triangle0 = subdivideTriangle(A, ab_mid, ca_mid, depth);
        short[] triangle1 = subdivideTriangle(ab_mid, B, bc_mid, depth);
        short[] triangle2 = subdivideTriangle(ca_mid, bc_mid, C, depth);
        short[] triangle3 = subdivideTriangle(ab_mid, bc_mid, ca_mid, depth);

        return ConcatTriangleLists(new short[][]{
                triangle0,
                triangle1,
                triangle2,
                triangle3,
        });
    }

    public void subdivide(int depth) {
        final Vector<Short> oldIndexes = indexes;
        indexes = new Vector<>((int) (indexes.size() * Math.pow(4, depth)), 3);

        for (int i=0; i<oldIndexes.size(); i+=3) {
            short[] newIndexes = subdivideTriangle(oldIndexes.elementAt(i), oldIndexes.elementAt(i+1), oldIndexes.elementAt(i+2), depth);

            for (short newIndex : newIndexes) {
                indexes.add(newIndex);
            }
        }
    }

    public void fixTextureCoordinates() {
        long index_size = indexes.size();
        for (int i=0; i<index_size; i+=3) {
            short A = indexes.elementAt(i);
            short B = indexes.elementAt(i+1);
            short C = indexes.elementAt(i+2);

            Vertex3 tA = textCoord.elementAt(A);
            Vertex3 tB = textCoord.elementAt(B);
            Vertex3 tC = textCoord.elementAt(C);

            float AB = tA.x - tB.x;
            float AC = tA.x - tC.x;
            float BC = tB.x - tC.x;
            boolean tABbig = Math.abs(AB) >= 0.5;
            boolean tACbig = Math.abs(AC) >= 0.5;
            boolean tBCbig = Math.abs(BC) >= 0.5;

            if (tABbig || tACbig || tBCbig) {
//                Log.e(TAG, "(" + A + ";" + B + ";" + C + ") -> (" + tA.x + ";" + tB.x + ";" + tC.x + ")");
            } else {
//                Log.v(TAG, "(" + A + ";" + B + ";" + C + ") -> (" + tA.x + ";" + tB.x + ";" + tC.x + ")");
                continue;
            }

            short A_dup = (short) vertices.size();
            short B_dup = (short) (A_dup+1);
            short C_dup = (short) (A_dup+2);

            try {
                vertices.add(vertices.elementAt(A).clone());
                vertices.add(vertices.elementAt(B).clone());
                vertices.add(vertices.elementAt(C).clone());
                tA = tA.clone();
                tB = tB.clone();
                tC = tC.clone();
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException("Clone should be supported here");
            }

//            int counter = 0; // possible values are 1 or 2
//            if (tABbig) counter++;
//            if (tACbig) counter++;
//            if (tBCbig) counter++;

            boolean increamentA = false;
            boolean increamentB = false;
            boolean increamentC = false;

            if (tABbig) {
                if (AB > 0) increamentB = true;
                else increamentA = true;
            }
            if (tACbig) {
                if (AC > 0) increamentC = true;
                else increamentA = true;
            }
            if (tBCbig) {
                if (BC > 0) increamentC = true;
                else increamentB = true;
            }

            if (increamentA) tA.x +=1.0;
            if (increamentB) tB.x +=1.0;
            if (increamentC) tC.x +=1.0;

            textCoord.add(tA);
            textCoord.add(tB);
            textCoord.add(tC);

            indexes.set(i, A_dup);
            indexes.set(i+1, B_dup);
            indexes.set(i+2, C_dup);
        }
    }
}
