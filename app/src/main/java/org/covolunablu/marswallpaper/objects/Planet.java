package org.covolunablu.marswallpaper.objects;

import android.content.Context;
import android.opengl.GLES20;

import org.covolunablu.marswallpaper.MyGLRenderer;
import org.covolunablu.marswallpaper.R;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by matteo on 10/8/17.
 */

public class Planet extends RenderableObject {
    private static final String TAG = "Planet";
    protected final FloatBuffer textCoordBuffer;

    private int mMVMatrixHandle;
    private int mNormalMatrixHandle;
    private int mColorTextureUniformHandle;
    private int mNormalTextureUniformHandle;

    private int mColorTextureDataHandle;
    private int mNormalTextureDataHandle;

    public enum PlanetEnum {
        MOON,
        MARS,
        MERCURY,
        JUPITER,
        VENUS,
        SATURN,
        EARTH,
    }

    public static PlanetEnum PlanetEnumFromString(String name)
    {
        switch (name) {
            case "MOON":    return PlanetEnum.MOON;
            case "MARS":    return PlanetEnum.MARS;
            case "MERCURY": return PlanetEnum.MERCURY;
            case "JUPITER": return PlanetEnum.JUPITER;
            case "VENUS":   return PlanetEnum.VENUS;
            case "SATURN":  return PlanetEnum.SATURN;
            case "EARTH":   return PlanetEnum.EARTH;
            default:
                throw new RuntimeException("Unrecognized String for PlanetEnum");
        }
    }

    private PlanetEnum planetType;

    public void setPlanetType(Context context, String planetType)
    {
        setPlanetType(context, PlanetEnumFromString(planetType));
    }

    public void setPlanetType(Context context, PlanetEnum planetType)
    {
        if (this.planetType == planetType) {
            return;
        }

        switch (planetType) {
            case MOON:
                this.vertexShaderAsset   = MoonAssets.VERTEX_SHADER_ASSET;
                this.fragmentShaderAsset = MoonAssets.FRAGMENT_SHADER_ASSET;
                this.colorTextureAsset   = MoonAssets.COLOR_TEXTURE_ASSET;
                this.normalTextureAsset  = MoonAssets.NORMAL_TEXTURE_ASSET;
                break;
            case MARS:
                this.vertexShaderAsset   = MarsAssets.VERTEX_SHADER_ASSET;
                this.fragmentShaderAsset = MarsAssets.FRAGMENT_SHADER_ASSET;
                this.colorTextureAsset   = MarsAssets.COLOR_TEXTURE_ASSET;
                this.normalTextureAsset  = MarsAssets.NORMAL_TEXTURE_ASSET;
                break;
            case MERCURY:
            case JUPITER:
            case VENUS:
            case SATURN:
            case EARTH:
            default:
                throw new RuntimeException("Planet not implemented yet");
        }

        this.planetType = planetType;
        initShaderProgram(context);
        mColorTextureDataHandle = loadColorTexture(context);
        mNormalTextureDataHandle = loadNormalTexture(context);
    }

    private class MarsAssets {
        private static final String VERTEX_SHADER_ASSET = "shaders/mars.vert";
        private static final String FRAGMENT_SHADER_ASSET = "shaders/mars.frag";
        private static final int COLOR_TEXTURE_ASSET = R.mipmap.mars_1k_color;
        private static final int NORMAL_TEXTURE_ASSET = R.mipmap.mars_1k_normal;
    }

    private class MoonAssets {
        private static final String VERTEX_SHADER_ASSET = "shaders/moon.vert";
        private static final String FRAGMENT_SHADER_ASSET = "shaders/moon.frag";
        private static final int COLOR_TEXTURE_ASSET = R.mipmap.moonmap2k;
        private static final int NORMAL_TEXTURE_ASSET = R.mipmap.moonnormal2k;
    }

    private String vertexShaderAsset;
    private String fragmentShaderAsset;
    private int    colorTextureAsset;
    private int    normalTextureAsset;

    public static Planet GeneratePlanet(final Context context, PlanetEnum planetEnum) {
        float phi = (float) ((1.0d + Math.sqrt(5.0f)) / 2.0d);    //1.618
        float du  = (float) (1.0d / Math.sqrt(phi * phi + 1.0d)); //0.526
        float dv  = phi * du; // 0.85065

        float[] vertexArray = {
                0.0f,   dv,   du, // 0
                0.0f,   dv,  -du, // 1
                0.0f,  -dv,   du, // 2
                0.0f,  -dv,  -du, // 3
                  du, 0.0f,   dv, // 4
                 -du, 0.0f,   dv, // 5
                  du, 0.0f,  -dv, // 6
                 -du, 0.0f,  -dv, // 7
                  dv,   du, 0.0f, // 8
                  dv,  -du, 0.0f, // 9
                 -dv,   du, 0.0f, // 10
                 -dv,  -du, 0.0f, // 11
        };

        short[] indexArray = {
                0,  1,  8,
                0,  4,  5,
                0,  5, 10,
                0,  8,  4,
                0, 10,  1,
                1,  6,  8,
                1,  7,  6,
                1, 10,  7,
                2,  3, 11,
                2,  4,  9,
                2,  5,  4,
                2,  9,  3,
                2, 11,  5,
                3,  7, 11,
                3,  6,  7,
                3,  9,  6,
                4,  8,  9,
                5, 11, 10,
                6,  9,  8,
                7, 10, 11,
        };


        VertexList vertexList = new VertexList(vertexArray, indexArray);

        vertexList.subdivide(3);
        vertexList.fixTextureCoordinates();

        switch (planetEnum) {
            case MOON:
                return new Planet(
                        context,
                        vertexList.generateVertexData(),
                        vertexList.generateTextCoordData(),
                        vertexList.generateIndexesData(),
                        PlanetEnum.MOON,
                        MoonAssets.VERTEX_SHADER_ASSET,
                        MoonAssets.FRAGMENT_SHADER_ASSET,
                        MoonAssets.COLOR_TEXTURE_ASSET,
                        MoonAssets.NORMAL_TEXTURE_ASSET
                );
            case MARS:
                return new Planet(
                        context,
                        vertexList.generateVertexData(),
                        vertexList.generateTextCoordData(),
                        vertexList.generateIndexesData(),
                        PlanetEnum.MARS,
                        MarsAssets.VERTEX_SHADER_ASSET,
                        MarsAssets.FRAGMENT_SHADER_ASSET,
                        MarsAssets.COLOR_TEXTURE_ASSET,
                        MarsAssets.NORMAL_TEXTURE_ASSET
                );
            case MERCURY:
            case JUPITER:
            case VENUS:
            case SATURN:
            case EARTH:
            default:
                throw new RuntimeException("Planet not implemented yet");
        }
    }

    private Planet(final Context context,
                   float[] vertexArray,
                   float[] textCoordArray,
                   short[] indexArray,
                   final PlanetEnum planetType,
                   final String vertexShaderAsset,
                   final String fragmentShaderAsset,
                   final int colorTextureAsset,
                   final int normalTextureAsset)
    {
        super(vertexArray, indexArray);
        this.planetType = planetType;
        this.vertexShaderAsset = vertexShaderAsset;
        this.fragmentShaderAsset = fragmentShaderAsset;
        this.colorTextureAsset = colorTextureAsset;
        this.normalTextureAsset = normalTextureAsset;
        initShaderProgram(context);

        // initialize uv coordinates byte buffer
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinates values * 4 bytes per float)
                textCoordArray.length * 4
        );
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        textCoordBuffer = bb.asFloatBuffer();
        // ad the colors to the FloatBuffer
        textCoordBuffer.put(textCoordArray);
        // set the buffer to read the first coordinate
        textCoordBuffer.position(0);

        mColorTextureDataHandle = loadColorTexture(context);
        mNormalTextureDataHandle = loadNormalTexture(context);
    }

    @Override
    String GetVertexShaderCode(final Context context) {
        return getShaderCodeFromAsset(context, vertexShaderAsset);
    }

    @Override
    String GetFragmentShaderCode(final Context context) {
        return getShaderCodeFromAsset(context, fragmentShaderAsset);
    }

    private int loadColorTexture(final Context context)
    {
        return MyGLRenderer.loadTexture(context, colorTextureAsset);
    }

    private int loadNormalTexture(final Context context)
    {
        return MyGLRenderer.loadTexture(context, normalTextureAsset);
    }


    @Override
    protected void populateHandles() {
        // attributes
        mPositionHandle  = GLES20.glGetAttribLocation(mProgram, "v_Position");
        mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgram, "v_TextureCoordinate");
        // uniforms
        mMVMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVMatrix");
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVPMatrix");
        mNormalMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_NormalMatrix");
        mColorTextureUniformHandle = GLES20.glGetUniformLocation(mProgram, "u_ColorTexture");
        mNormalTextureUniformHandle = GLES20.glGetUniformLocation(mProgram, "u_NormalTexture");
    }

    private static final int COORDS_PER_TEXTURE_COORD = 2;
    private        final int textCoordStride          = COORDS_PER_TEXTURE_COORD * 4; // 4 bytes per float

    @Override
    public void draw(float[] MVMatrix, float[] MVPMatrix, float[] NormalMatrix) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);
        // get handle to vertex shader's vPosition member

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(
                mPositionHandle,
                COORDS_PER_VERTEX,
                GLES20.GL_FLOAT,
                false,
                vertexStride,
                vertexBuffer
        );
        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);
        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(
                mTextureCoordinateHandle,
                COORDS_PER_TEXTURE_COORD,
                GLES20.GL_FLOAT,
                false,
                textCoordStride,
                textCoordBuffer
        );

        // Set texture
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0); // Set the active texture unit to texture unit 0
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mColorTextureDataHandle);
        GLES20.glUniform1i(mColorTextureUniformHandle, 0);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE1); // Set the active texture unit to texture unit 0
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mNormalTextureDataHandle);
        GLES20.glUniform1i(mNormalTextureUniformHandle, 1);

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVMatrixHandle, 1, false, MVMatrix, 0);
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, MVPMatrix, 0);
        GLES20.glUniformMatrix4fv(mNormalMatrixHandle, 1, false, NormalMatrix, 0);

        // Draw the triangle
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, vertexCount, GLES20.GL_UNSIGNED_SHORT, drawListBuffer);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
        GLES20.glDisableVertexAttribArray(mTextureCoordinateHandle);
    }
}
