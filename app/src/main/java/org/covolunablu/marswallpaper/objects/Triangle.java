package org.covolunablu.marswallpaper.objects;

import android.content.Context;

/**
 * Created by matteo on 10/8/17.
 */

public class Triangle extends RenderableObject {
    private static final String TAG = "Triangle";

    private static float TriangleCoords[] = { // in couterclockwise order:
             0.0f*2,  0.622008459f*2, 0.0f*2, // top
            -0.5f*2, -0.311004243f*2, 0.0f*2, // bottom left
             0.5f*2, -0.311004243f*2, 0.0f*2  // bottom right
    };

    public Triangle(final Context context) {
        super(TriangleCoords);
        initShaderProgram(context);
    }

    @Override
    String GetVertexShaderCode(final Context context) {
        return getShaderCodeFromAsset(context, "shaders/basic.vert");
    }

    @Override
    String GetFragmentShaderCode(final Context context) {
        return getShaderCodeFromAsset(context, "shaders/basic.frag");
    }
}
