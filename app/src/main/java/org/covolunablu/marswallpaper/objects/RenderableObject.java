package org.covolunablu.marswallpaper.objects;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import org.covolunablu.marswallpaper.MyGLRenderer;
import org.covolunablu.androidtools.Helper;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by matteo on 10/8/17.
 */

abstract public class RenderableObject {
    private static final String TAG = "RenderableObject";

    protected final FloatBuffer vertexBuffer;
    protected final ShortBuffer drawListBuffer;
    protected int mVertexShader = -1;
    protected int mFragmentShader = -1;
    protected int mProgram = -1;

    String getShaderCodeFromAsset(final Context context, String assetName)
    {
        try {
            return Helper.LoadStringAsset(context.getAssets(), assetName);
        } catch (IOException e) {
            throw new RuntimeException("Unable to load shader \"" + assetName + "\" from assets:\n" + e);
        }
    }
    abstract String GetVertexShaderCode(final Context context);
    abstract String GetFragmentShaderCode(final Context context);
    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;

    // Set color with red, green, blue and alpha values
    float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 1.0f };

    public RenderableObject(float[] vertexCoordinates)
    {
        this(vertexCoordinates, null);
    }

    public RenderableObject(float[] vertexCoordinates, short[] drawOrder)
    {
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinates values * 4 bytes per float)
                vertexCoordinates.length * 4
        );
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        // ad the coordinates to the FloatBuffer
        vertexBuffer.put(vertexCoordinates);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0);

        if (drawOrder != null) {
            // initialize byte buffer for the draw list
            ByteBuffer dlb = ByteBuffer.allocateDirect(
                    // (# of coordinate values * 2 bytes per short)
                    drawOrder.length * 2
            );
            dlb.order(ByteOrder.nativeOrder());
            drawListBuffer = dlb.asShortBuffer();
            drawListBuffer.put(drawOrder);
            drawListBuffer.position(0);

            vertexCount = drawOrder.length;
        } else {
            drawListBuffer = null;
            vertexCount = vertexCoordinates.length / COORDS_PER_VERTEX;
        }
    }

    protected void initShaderProgram(final Context context)
    {
        if (mVertexShader < 0) {
            GLES20.glDeleteShader(mVertexShader);
        }
        if (mFragmentShader < 0) {
            GLES20.glDeleteShader(mFragmentShader);
        }

        mVertexShader = MyGLRenderer.loadShader(GLES20.GL_VERTEX_SHADER, GetVertexShaderCode(context));
        mFragmentShader = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, GetFragmentShaderCode(context));

        if (mProgram > 0) {
            GLES20.glDeleteProgram(mProgram);
        }

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();
        if (mProgram != 0) {
            GLES20.glAttachShader(mProgram, mVertexShader);
            GLES20.glAttachShader(mProgram, mFragmentShader);
            GLES20.glLinkProgram(mProgram);

            int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(mProgram, GLES20.GL_LINK_STATUS, linkStatus, 0);
            if (linkStatus[0] != GLES20.GL_TRUE) {
                String error_message = "Could not link program: " + GLES20.glGetProgramInfoLog(mProgram);
                Log.e(TAG, error_message);
                GLES20.glDeleteProgram(mProgram);

                throw new RuntimeException(error_message);
            }
        }

        populateHandles();
    }

    protected void populateHandles() {
        // attributes
        mPositionHandle  = GLES20.glGetAttribLocation(mProgram, "v_Position");
        // uniforms
        mTextureCoordinateHandle = GLES20.glGetUniformLocation(mProgram, "v_Color");
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "u_MVPMatrix");
    }

    protected int mMVPMatrixHandle;
    protected int mPositionHandle;
    protected int mTextureCoordinateHandle;

    protected final int vertexCount;
    protected final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per float

    public void draw(float[] MVMatrix, float[] MVPMatrix, float[] NormalMatrix) {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);
        // get handle to vertex shader's vPosition member

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(
                mPositionHandle,
                COORDS_PER_VERTEX,
                GLES20.GL_FLOAT,
                false,
                vertexStride,
                vertexBuffer
        );

        // Set color for drawing the triangle
        GLES20.glUniform4fv(mTextureCoordinateHandle, 1, color, 0);

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, MVPMatrix, 0);

        // Draw the triangle
        if (drawListBuffer == null) {
            GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);
        } else {
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, vertexCount, GLES20.GL_UNSIGNED_SHORT, drawListBuffer);
        }

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }
}
