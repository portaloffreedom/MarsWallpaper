package org.covolunablu.marswallpaper.activities;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;

import org.covolunablu.marswallpaper.MarsWallpaperService;
import org.covolunablu.marswallpaper.R;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by matteo on 28/12/17.
 */

public class MarsWallpaperHome extends Activity
{
    private static final String TAG = "MarsWallpaperHome";
    private OrientationEventListener mOrientationListener;
    private int                      lastOrientation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        refitOrientation();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mOrientationListener = new OrientationEventListener(this)
            {
                @Override
                public void onOrientationChanged(int orientation)
                {
                    orientation = getScreenOrientation();
                    if (orientation != lastOrientation) {
                        refitOrientation();
                    }
                }
            };

            if (mOrientationListener.canDetectOrientation()) {
                mOrientationListener.enable();
            } else {
                mOrientationListener.disable();
                mOrientationListener = null;
            }
        }

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (mOrientationListener != null) {
            mOrientationListener.disable();
        }
    }

    protected void refitOrientation()
    {
        View rootView = findViewById(R.id.home_root_view);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            rootView.setPadding(0, 0, 0, 0);
            return;
        }

        int statusBarHeight = getStatusBarHeight();
        statusBarHeight = statusBarHeight < 0 ? 24 : statusBarHeight;

        Point navBarDims = getNavigationBarSize(this);

        if (navBarDims == null) {
            rootView.setPadding(0, statusBarHeight, 0, 0);
        } else {
            int orientation = getScreenOrientation();
            lastOrientation = orientation;
            if (orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT || orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT) {
                rootView.setPadding(0, statusBarHeight, 0, navBarDims.y);
            } else if (orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                rootView.setPadding(0, statusBarHeight, navBarDims.x, 0);
            } else if (orientation == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
                rootView.setPadding(navBarDims.x, statusBarHeight, 0, 0);
            }
        }
    }

    public void onClick(View view)
    {
        Intent intent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent = new Intent(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
            intent.putExtra(
                    WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT,
                    new ComponentName(this, MarsWallpaperService.class)
            );
        } else {
            intent = new Intent(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
        }
        startActivity(intent);
    }

    private int getNavBarHeight()
    {
        return getNavigationBarSize(this).y;
    }

    private int getStatusBarHeight()
    {
        return getPrivateDimension("status_bar_height");
    }

    private int getPrivateDimension(String resource_name)
    {
        Resources resources  = getResources();
        int       resourceId = resources.getIdentifier(resource_name, "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return -1;
    }

    public static Point getNavigationBarSize(Context context)
    {
        Point appUsableSize  = getAppUsableScreenSize(context);
        Point realScreenSize = getRealScreenSize(context);

        // navigation bar on the right
        if (appUsableSize.x < realScreenSize.x) {
            return new Point(realScreenSize.x - appUsableSize.x, appUsableSize.y);
        }

        // navigation bar at the bottom
        if (appUsableSize.y < realScreenSize.y) {
            return new Point(appUsableSize.x, realScreenSize.y - appUsableSize.y);
        }

        // navigation bar is not present
        return null;
    }

    public static Point getAppUsableScreenSize(Context context)
    {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Point         size          = new Point();

        if (windowManager != null) {
            Display display = windowManager.getDefaultDisplay();
            display.getSize(size);
        }
        return size;
    }

    public static Point getRealScreenSize(Context context)
    {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Point         size          = new Point();

        if (windowManager != null) {
            Display display = windowManager.getDefaultDisplay();
            if (Build.VERSION.SDK_INT >= 17) {
                display.getRealSize(size);
            } else {
                try {
                    size.x = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                    size.y = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
                } catch (IllegalAccessException ignored) {
                } catch (InvocationTargetException ignored) {
                } catch (NoSuchMethodException ignored) {
                }
            }
        }

        return size;
    }

    private int getScreenOrientation()
    {
        int            rotation = getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm       = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width  = dm.widthPixels;
        int height = dm.heightPixels;
        int orientation;
        // if the device's natural orientation is portrait:
        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width ||
                (rotation == Surface.ROTATION_90
                        || rotation == Surface.ROTATION_270) && width > height)
        {
            switch (rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_180:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_270:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                default:
                    Log.e(TAG, "Unknown screen orientation. Defaulting to " +
                            "portrait.");
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
            }
        }
        // if the device's natural orientation is landscape or if the device
        // is square:
        else {
            switch (rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case Surface.ROTATION_270:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                default:
                    Log.e(TAG, "Unknown screen orientation. Defaulting to " +
                            "landscape.");
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
            }
        }

        return orientation;
    }

    public void onClickInfo(View view)
    {
        Intent intent = new Intent(this, MarsWallpaperInfo.class);
        startActivity(intent);
    }
}
