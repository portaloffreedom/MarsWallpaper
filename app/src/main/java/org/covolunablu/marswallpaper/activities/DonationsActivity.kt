/*
 * Copyright (C) 2011-2015 Dominik Schürmann <dominik@dominikschuermann.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// file modified from https://github.com/PrivacyApps/donations/blob/aa4b03a15efcba020221c914272ab35dbf043ddb/example/src/main/java/org/sufficientlysecure/donations/example/DonationsActivity.java

package org.covolunablu.marswallpaper.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import org.covolunablu.marswallpaper.BuildConfig
import org.covolunablu.marswallpaper.R
import org.sufficientlysecure.donations.DonationsFragment

class DonationsActivity : FragmentActivity() {

    /**
     * Called when the activity is first created.
     */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_donations)
        actionBar?.setDisplayShowTitleEnabled(true)

        val ft = supportFragmentManager.beginTransaction()
        val donationsFragment: DonationsFragment
        if (BuildConfig.DONATIONS_GOOGLE) {
            donationsFragment = DonationsFragment.newInstance(false,
                    true,
                    GOOGLE_PUBKEY,
                    GOOGLE_CATALOG,
                    resources.getStringArray(R.array.donation_google_catalog_values),
                    false,
                    null,
                    null,
                    null,
                    false,
                    null,
                    null,
                    false,
                    null)
        } else {
            donationsFragment = DonationsFragment.newInstance(BuildConfig.DEBUG,
                    false,
                    null,
                    null,
                    null,
                    true,
                    PAYPAL_USER,
                    PAYPAL_CURRENCY_CODE,
                    getString(R.string.donation_paypal_item),
                    false,
                    FLATTR_PROJECT_URL,
                    FLATTR_URL,
                    false,
                    BITCOIN_ADDRESS)
        }

        ft.replace(R.id.donations_activity_container, donationsFragment, "donationsFragment")
        ft.commit()
    }

    /**
     * Needed for Google Play In-app Billing. It uses startIntentSenderForResult(). The result is not propagated to
     * the Fragment like in startActivityForResult(). Thus we need to propagate manually to our Fragment.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        val fragmentManager = supportFragmentManager
        val fragment = fragmentManager.findFragmentByTag("donationsFragment")
        fragment?.onActivityResult(requestCode, resultCode, data)
    }

    companion object {

        /**
         * Google
         */
        private const val GOOGLE_PUBKEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnYLx+sRdFJIEyHYNDqqWX0AfSf2dsR1wCtE7BZsINaTgX9ATh7O5m50OM2WfnHXos1yA6VKtpVzcStDiGyPe1YHlCjwhlztYnV06wl0zAmatG6dlMFUyTj1duRGKxSaPvaKL2KKIjBC9HCvZFOnbCWKv+2yrMR0ZO/GZXVpJMHf1j9Lhe7LpE1IticNC6WKumIOKAIFFIICaBMpQUg36DnzDAmOwVBs9M/7oB0779e7uoj6yt4+f00FlS88WFNSj0Bwdc8POyO1lXtiFlt/xaTVAiWaHhM9g44V2/Aua1GpRwqdXr6V2OLKBVlmkIFXge2kd58EKe9uVqhS/4U2v+wIDAQAB"
        private val GOOGLE_CATALOG = arrayOf(
                "org.covolunablu.marswallpaper.donation.1",
                "org.covolunablu.marswallpaper.donation.2",
                "org.covolunablu.marswallpaper.donation.3",
                "org.covolunablu.marswallpaper.donation.5",
                "org.covolunablu.marswallpaper.donation.8",
                "org.covolunablu.marswallpaper.donation.13")

        /**
         * PayPal
         */
        private const val PAYPAL_USER = "matteo.dek@covolunablu.org"
        private const val PAYPAL_CURRENCY_CODE = "EUR"

        /**
         * Flattr
         */
        private const val FLATTR_PROJECT_URL = "https://git.covolunablu.org/portaloffreedom/MarsWallpaper"
        // FLATTR_URL without http:// !
        private const val FLATTR_URL = "flattr.com/@portaloffreedom"

        /**
         * Bitcoin
         */
        private const val BITCOIN_ADDRESS = ""
    }

}