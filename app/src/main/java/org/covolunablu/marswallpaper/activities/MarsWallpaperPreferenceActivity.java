package org.covolunablu.marswallpaper.activities;

import android.content.Context;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import org.covolunablu.marswallpaper.R;

/**
 * Created by matteo on 10/8/17.
 */

public class MarsWallpaperPreferenceActivity extends PreferenceActivity
{
    private static final String TAG          = "MarsWallpaperPreference";
    private static final String INT_REGEXP   = "\\d*";
    private static final String FLOAT_REGEXP = "([0-9]*[.])?[0-9]+";

    private float refreshRating;

    public MarsWallpaperPreferenceActivity()
    {
        this.refreshRating = 60;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            Display display = windowManager.getDefaultDisplay();
            this.refreshRating = display.getRefreshRate();
            Log.d(TAG, "detected display refresh rate of " + this.refreshRating);
        }

        //this.requestWindowFeature(Window.FEATURE_ACTION_BAR);
        addPreferencesFromResource(R.xml.prefs);

        // add a validator to the preferences so that it only accepts valid numbers
        EditTextPreference fpsPreference              = (EditTextPreference) getPreferenceScreen().findPreference("fps");
        EditTextPreference speedMultiplayerPreference = (EditTextPreference) getPreferenceScreen().findPreference("speedup_multiplier");

        // add the validator
        fpsPreference.setOnPreferenceChangeListener(fpsCheckListener);
        speedMultiplayerPreference.setOnPreferenceChangeListener(speedMultiplierCheckListener);
    }

    /**
     * Checks that the FPS value is valid
     */
    Preference.OnPreferenceChangeListener fpsCheckListener = new Preference.OnPreferenceChangeListener()
    {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue)
        {
            // check that the string is an integer
            try {

                if (newValue != null
                        && newValue.toString().length() > 0
                        && newValue.toString().matches(FLOAT_REGEXP))
                {
                    float parsedValue = Float.parseFloat(newValue.toString());
                    if (parsedValue >= 0 && parsedValue <= refreshRating) {
                        return true;
                    }
                }

            } catch (NumberFormatException ignored) {
            }

            // If not an integer, create a message to the user
            Toast.makeText(
                    MarsWallpaperPreferenceActivity.this,
                    "Invalid FPS value",
                    Toast.LENGTH_SHORT
            ).show();

            return false;
        }
    };

    /**
     * Checks that the speed multiplier value is valid
     */
    Preference.OnPreferenceChangeListener speedMultiplierCheckListener = new Preference.OnPreferenceChangeListener()
    {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue)
        {
            // check that the string is an integer
            try {

                if (newValue != null
                        && newValue.toString().length() > 0
                        && newValue.toString().matches(FLOAT_REGEXP))
                {
                    float parsedValue = Float.parseFloat(newValue.toString());
                    if (parsedValue >= 0) {
                        return true;
                    }
                }

            } catch (NumberFormatException ignored) {
            }

            // If not an integer, create a message to the user
            Toast.makeText(
                    MarsWallpaperPreferenceActivity.this,
                    "Invalid Speed Multiplier value",
                    Toast.LENGTH_SHORT
            ).show();

            return false;
        }
    };
}
