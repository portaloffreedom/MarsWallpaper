package org.covolunablu.marswallpaper.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MotionEvent;

import org.covolunablu.marswallpaper.MyGLSurfaceView;

/**
 * Created by matteo on 10/8/17.
 */

public class ShowCaseActivity extends Activity {
    private static final String TAG = "ShowCaseActivity";
    private MyGLSurfaceView mGLView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Create a GLSurfaceView instance and set it
        // as the ContextView for this Activity
        mGLView = new MyGLSurfaceView(this);
        setContentView(mGLView);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float relativePosition = event.getX()/mGLView.getWidth();
        mGLView.setOffset(relativePosition, 0.5f);

        return super.onTouchEvent(event);
    }
}
