package org.covolunablu.marswallpaper.activities

import android.os.Bundle
import android.app.Activity
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.widget.Toast
import org.covolunablu.marswallpaper.R

import kotlinx.android.synthetic.main.activity_mars_wallpaper_info.*
import android.view.LayoutInflater
import android.webkit.WebView



class MarsWallpaperInfo : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mars_wallpaper_info)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.title = getString(R.string.about_title)
        actionBar.setDisplayShowTitleEnabled(true)

        show_license.setOnClickListener { showLicense("file:///android_asset/licenses/gpl-3.0-standalone.html") }
        show_third_party_licenses.setOnClickListener { showLicense("file:///android_asset/licenses/third-party.html") }
        show_donate.setOnClickListener { showDonate() }
    }

    private fun showLicense(licenseURL: String) {
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_licenses, null) as WebView
        view.loadUrl(licenseURL)
        AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert)
//                .setTitle(getString(R.string.license))
                .setView(view)
                .setPositiveButton(android.R.string.ok, null)
                .show()
    }

    private fun showDonate() {
        val intent = Intent(this, DonationsActivity::class.java)
        startActivity(intent)
    }

}
