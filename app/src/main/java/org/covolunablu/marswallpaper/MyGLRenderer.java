package org.covolunablu.marswallpaper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import org.covolunablu.marswallpaper.objects.Planet;
import org.covolunablu.marswallpaper.objects.SkyMap;
import org.covolunablu.opengltools.MatrixStack;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by matteo on 25/12/17.
 */

public class MyGLRenderer implements GLSurfaceView.Renderer
{
    private static final String TAG = "MyGLRenderer";

    // Android context
    Context mContext;

    // mVPMatrix is an abbreviation for "Model View Projection Matrix"
    private final float[]     mMVPMatrix              = new float[16];
    private final float[]     mProjectionMatrix       = new float[16];
    private final float[]     mSkyMapProjectionMatrix = new float[16];
    private final float[]     mModelViewMatrix        = new float[16];
    private final float[]     mNormalMatrix           = new float[16];
    private final float[]     tmpMatrix               = new float[16];
    private final MatrixStack mModelViewMatrixStack   = new MatrixStack();

    // Objects
    private Planet mPlanet;
    private SkyMap mSkyMap;

    // Rendering variables
    private float  xOffset;
    private float  yOffset;
    private double rotation_speedup;
    private long   start_time;

    public MyGLRenderer(Context context)
    {
        this.mContext = context;
        this.start_time = -1;
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig eglConfig)
    {
        // Set the background frame color
        GLES20.glClearColor(0, 0, 0, 0);
        // Set culling
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glCullFace(GLES20.GL_FRONT);
        // Set depth testing
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glDepthFunc(GLES20.GL_LEQUAL);
        GLES20.glDepthMask(true);

        // initialize planet
        String planetTypeString = PreferenceManager
                .getDefaultSharedPreferences(mContext)
                .getString("planet", "MARS");
        Planet.PlanetEnum planetType = Planet.PlanetEnumFromString(planetTypeString);
        mPlanet = Planet.GeneratePlanet(this.mContext, planetType);
        //mSkyMap = SkyMap.GenerateSkyMap(this.mContext); delayed load
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        GLES20.glViewport(0, 0, width, height);

        float ratio = (float) width / height;

        Matrix.perspectiveM(mSkyMapProjectionMatrix, 0, 90f, ratio, 0.01f, 300f);
        Matrix.perspectiveM(mProjectionMatrix, 0, 2, ratio, 0.01f, 300f);
    }

    @Override
    public void onDrawFrame(GL10 gl)
    {
        String planetType = PreferenceManager
                .getDefaultSharedPreferences(mContext)
                .getString("planet", "MARS");
        String skymap_res = PreferenceManager
                .getDefaultSharedPreferences(mContext)
                .getString("starmap_res", "2");

        boolean render_sky = !skymap_res.equals("0");

        // Clear color and depth buffer
        if (render_sky) {
            GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT);
        } else {
            GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
        }

        // Set planet type (automatic check if change are necessary inside)
        mPlanet.setPlanetType(mContext, planetType);

        // planet rotation
        long time = getTimePassed();
        // a day on mars lasts 24h 39m 35.244147s
        // 360/((24*60*60)+(39*60)+35.244147) = 0.00405518456 degrees per second in rotation
        double angle = (0.00405518456d * ((double) time) / 1000d);
        // real rotation is too slow, let's speed it up:
        rotation_speedup = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(mContext).getString("speedup_multiplier", "100"));
        angle = angle * rotation_speedup;

        // OBJECTS RENDERING
        mModelViewMatrixStack.glPushMatrix();
        mModelViewMatrixStack.glTranslatef(0, 0, -150f);

        // axis inclination
        mModelViewMatrixStack.glRotatef(5.65f, 0.5f, 0, 0.5f);

        // planet rotation
        mModelViewMatrixStack.glRotatef((float) (angle - (xOffset * 90)), 0, 1.0f, 0);

        // Calculate the projection and the view transformation
        mModelViewMatrixStack.getMatrix(mModelViewMatrix, 0);
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mModelViewMatrix, 0);

        Matrix.invertM(tmpMatrix, 0, mModelViewMatrix, 0);
        Matrix.transposeM(mNormalMatrix, 0, tmpMatrix, 0);

        // Draw shape
        //mTriangle.draw(mModelViewMatrix, mMVPMatrix, mNormalMatrix);
        //mSquare.draw(mModelViewMatrix, mMVPMatrix, mNormalMatrix);
        mPlanet.draw(mModelViewMatrix, mMVPMatrix, mNormalMatrix);
        mModelViewMatrixStack.glPopMatrix();

        // SKYBOX
        if (render_sky) {
            if (mSkyMap == null) {
                mSkyMap = SkyMap.GenerateSkyMap(mContext, skymap_res);
            } else {
                mSkyMap.setSkymapRes(mContext, skymap_res);
            }
            mModelViewMatrixStack.glPushMatrix();
            GLES20.glCullFace(GLES20.GL_BACK);
            mModelViewMatrixStack.glRotatef((float) (angle / 2), 0, 1.0f, 0);
            // scale to avoid depth buffer overwrite of the planet
            mModelViewMatrixStack.glScalef(300.0f, 300.0f, 300.0f);

            mModelViewMatrixStack.getMatrix(mModelViewMatrix, 0);
            Matrix.multiplyMM(mMVPMatrix, 0, mSkyMapProjectionMatrix, 0, mModelViewMatrix, 0);
            mSkyMap.draw(null, mMVPMatrix, null);
            GLES20.glCullFace(GLES20.GL_FRONT);
            mModelViewMatrixStack.glPopMatrix();
        } else if (mSkyMap != null) {
            mSkyMap = null;
            // Let the memory be cleaned! It's can be heavy to load to be honest
        }

        // Print errors
        verifyGLErrors();
    }

    public static int loadShader(int type, String shaderCode)
    {
        // Create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);


        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }

    public static int loadTexture(final Context context, final int resourceId)
    {
        final int[] textureHandle = new int[1];
        GLES20.glGenTextures(1, textureHandle, 0);
        if (textureHandle[0] != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;

            // Read in the resource
            final Bitmap bitmap =
                    BitmapFactory.decodeResource(context.getResources(), resourceId, options);

            Log.v(TAG, "loading texture (id " + resourceId + ") bytes count: " + bitmap.getByteCount());

            // Bind to the texture in OpenGL
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            // Set filtering
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            // Set Texture Wrap
            //GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
            //GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            // Load the bitmap into the bound texture.
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            // Mipmapping
            //GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);

            // Recycle the bitmap, since its data has been loaded into OpenGL.
            bitmap.recycle();
        } else {
            throw new RuntimeException("Error loading texture.");
        }

        return textureHandle[0];
    }

    public static boolean verifyGLErrors()
    {
        int err = GLES20.GL_NO_ERROR;
        while ((err = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            String error_name;
            switch (err) {
                case GLES20.GL_INVALID_ENUM:
                    error_name = "GL_INVALID_ENUM";
                    break;
                case GLES20.GL_INVALID_VALUE:
                    error_name = "GL_INVALID_VALUE";
                    break;
                case GLES20.GL_INVALID_OPERATION:
                    error_name = "GL_INVALID_OPERATION";
                    break;
                //case GLES20.GL_STACK_OVERFLOW:
                //    error_name = "GL_STACK_OVERFLOW"; break;
                //case GLES20.GL_STACK_UNDERFLOW:
                //    error_name = "GL_STACK_UNDERFLOW"; break;
                case GLES20.GL_OUT_OF_MEMORY:
                    error_name = "GL_OUT_OF_MEMORY";
                    break;
                case GLES20.GL_INVALID_FRAMEBUFFER_OPERATION:
                    error_name = "GL_INVALID_FRAMEBUFFER_OPERATION";
                    break;
                //case GLES20.GL_CONTEXT_LOST:
                //    error_name = "GL_CONTEXT_LOST"; break;
                //case GLES20.GL_TABLE_TOO_LARGE:
                //    error_name = "GL_TABLE_TOO_LARGE"; break;
                default:
                    error_name = "UNKNOWN_ERROR";
                    break;
            }
            Log.e(TAG, "OpenGL Error: " + error_name);
        }
        return err != GLES20.GL_NO_ERROR;
    }

    public void setOffset(float xOffset, float yOffset)
    {
        this.xOffset = xOffset;
        this.yOffset = yOffset;
    }

    public long getTimePassed()
    {
        if (start_time < 0) {
            start_time = SystemClock.uptimeMillis();
            return 0;
        }

        return SystemClock.uptimeMillis() - start_time;
    }
}
