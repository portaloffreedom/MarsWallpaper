package org.covolunablu.marswallpaper;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.WindowManager;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by matteo on 25/12/17.
 */

public abstract class GLWallpaperService extends WallpaperService
{
    public class GLEngine extends Engine
    {
        private static final String TAG = "GLEngine";
        private final float        refreshRating;
        private       MyGLRenderer mRenderer;
        private       boolean      stopRendering;

        private   WallpaperGLSurfaceView glSurfaceView;
        protected boolean                rendererHasBeenSet;


        private double FPS;
        private final Timer timer = new Timer();

        public GLEngine()
        {
            getNewFPSPreference();
            stopRendering = true;

            WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Display display = windowManager.getDefaultDisplay();
                refreshRating = display.getRefreshRate();
            } else {
                refreshRating = -1;
            }
        }

        private double getNewFPSPreference()
        {
            FPS = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getString("fps", "10"));
            return FPS;
        }

        public class WallpaperGLSurfaceView extends GLSurfaceView
        {
            private static final String TAG = "WallpaperGLSurfaceView";

            public WallpaperGLSurfaceView(Context context)
            {
                super(context);
                Log.v(TAG, "WallpaperGLSurfaceView Constructor()");
            }

            @Override
            public SurfaceHolder getHolder()
            {
                return GLEngine.this.getSurfaceHolder();
            }


            public void onDestroy()
            {
                Log.v(TAG, "WallpaperGLSurfaceView onDestroy()");
                super.onDetachedFromWindow();
            }

            @Override
            public void onPause()
            {
                Log.v(TAG, "WallpaperGLSurfaceView onPause()");
                stopRendering = true;
                //scheduleBlackFrame();
                requestRender();
                super.onPause();
            }

            @Override
            public void onResume()
            {
                Log.v(TAG, "WallpaperGLSurfaceView onResume()");
                stopRendering = false;
                requestRender();
                scheduleNextFrame();
                super.onResume();
            }

            public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset)
            {
                mRenderer.setOffset(xOffset, yOffset);
                requestRender();
            }
        }

        class RequestFrameTask extends TimerTask
        {
            @Override
            public void run()
            {
                if (rendererHasBeenSet) {
                    glSurfaceView.requestRender();
                }
                scheduleNextFrame();
            }
        }

        private void scheduleNextFrame()
        {
            if (!stopRendering) {
                getNewFPSPreference();
                final long wait_time;
                if (Math.floor(FPS) >= Math.floor(refreshRating)) {
                    wait_time = 0;
                } else {
                    wait_time = (long) ((1.0 / FPS) * 1000);
                }
                timer.schedule(new RequestFrameTask(), wait_time);
            }
        }

        @Override
        public void onCreate(SurfaceHolder surfaceHolder)
        {
            super.onCreate(surfaceHolder);
            glSurfaceView = new WallpaperGLSurfaceView(GLWallpaperService.this);
        }

        @Override
        public void onVisibilityChanged(boolean visible)
        {
            super.onVisibilityChanged(visible);

            if (rendererHasBeenSet) {
                if (visible) {
                    glSurfaceView.onResume();
                } else {
                    glSurfaceView.onPause();
                }
            }
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();
            glSurfaceView.onDestroy();
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset)
        {
            if (PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getBoolean("offset", true)) {
                super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
                glSurfaceView.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
            }
        }


        protected void setRenderer(GLSurfaceView.Renderer renderer)
        {
            glSurfaceView.setRenderer(renderer);
            mRenderer = (MyGLRenderer) renderer;
            rendererHasBeenSet = true;
            glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
            glSurfaceView.requestRender();
            scheduleNextFrame();
        }

        protected void setEGLContextClientVersion(int version)
        {
            glSurfaceView.setEGLContextClientVersion(version);
        }

        protected void setPreserveEGLContextOnPause(boolean preserve)
        {
            glSurfaceView.setPreserveEGLContextOnPause(preserve);
        }

    }
}