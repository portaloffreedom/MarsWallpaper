package org.covolunablu.androidtools;

import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Created by matteo on 10/9/17.
 */

public class Helper {
    static public String LoadStringAsset(AssetManager assetManager, String assetName)
            throws IOException
    {
        final int           bufferSize = 1024;
        final char[]        buffer     = new char[bufferSize];
        final StringBuilder out        = new StringBuilder();

        InputStream shaderAsset = assetManager.open(assetName);
        Reader      inputStream = new InputStreamReader(shaderAsset, "UTF-8");

        for (; ; ) {
            int rsz = inputStream.read(buffer, 0, buffer.length);
            if (rsz < 0)
                break;
            out.append(buffer, 0, rsz);
        }
        return out.toString();
    }
}
